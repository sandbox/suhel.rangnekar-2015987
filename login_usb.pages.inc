<?php
//$Id$
/**
*@file
*
*Menu callbacks for stalker module
*/
/**
*Page callback function for user/securityker page
*/

 /**
 * Form builder; Request a password reset.
 *
 * @ingroup forms
 * @see user_pass_validate()
 * @see user_pass_submit()
 */
 
 function securitykey(){

  $form['keyname'] = array(
    '#type' => 'textfield',
    '#title' => t('Username or e-mail address'),
    '#size' => 60,
    '#maxlength' => max(USERNAME_MAX_LENGTH, EMAIL_MAX_LENGTH),
    '#required' => TRUE,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('E-mail remove securitykey'));
  return $form;
 }
 
 function securitykey_validate($form, &$form_state) {
  $name = trim($form_state['values']['name']);
  

  // Try to load by email.
  $account = user_load(array('mail' => $name, 'status' => 1));
  if (!$account) {
    // No success, try to load by name.
    $account = user_load(array('name' => $name, 'status' => 1));
  }
  if ($account) {
    // Blocked accounts cannot request a new password,
    // check provided username and email against access rules.
    if (drupal_is_denied('user', $account->name) || drupal_is_denied('mail', $account->mail)) {
      form_set_error('name', t('%name is not allowed to request a new password.', array('%name' => $name)));
    }
  }
  if (isset($account->uid)) {
    form_set_value(array('#parents' => array('account')), $account, $form_state);
  }
  else {
    form_set_error('name', t('Sorry, %name is not recognized as a user name or an e-mail address.', array('%name' => $name)));
  }
}

function securitykey_submit($form, &$form_state) {
  global $language;

  $account = $form_state['values']['account'];
  // Mail one time login URL and instructions using current language. 
  //watchdog('user', 'Password reset instructions mailed to %name at %email.', array('%name' => $account->name, '%email' => $account->mail));
  
  drupal_set_message(t('Further instructions have been sent to your e-mail address.'));

  $form_state['redirect'] = 'user';
  return;
}

function securitykey_mail(){

}
